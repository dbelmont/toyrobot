﻿using System;
using ToyRobot;
using ToyRobot.Exceptions;

namespace ToyRobotCommand
{
	class Program
	{
		static readonly string[] _commands = { "PLACE 0,0,F", "MOVE", "LEFT", "RIGHT", "REPORT" };
		
		public static void Main(string[] args)
		{
			MainMenu();
		}
		
		static void MainMenu()
		{
			Console.WriteLine("Welcome to ToyRobot application\r\n");
			Console.WriteLine("Do you wish to:");
			Console.WriteLine("1 - Type commands to your ToyRobot");
			Console.WriteLine("2 - Import a file with commands to your ToyRobot");
			Console.Write("Type the number of your choice:");
			var choice = Console.ReadKey().KeyChar;
			
			Console.Clear();
			
			if (choice == '1')
				TypeCommands();
			else if (choice == '2')
				ReadCommandsFile();
			else
			{
				MainMenu();
			}
		}
		
		static void TypeCommands()
		{
			Console.WriteLine("Write the commands you wish to give to your ToyRobot and press 'Enter' to execute the command\r\nOr type 'QUIT' to quit the application.");
			var command = Console.ReadLine();
			var robot = new Robot();
			while (StringComparer.InvariantCultureIgnoreCase.Compare(command, "quit") != 0)
			{
				ExecuteCommand(robot, command);
				command = Console.ReadLine();
			}
		}
		
		static void ExecuteCommand(Robot robot, string command)
		{
			var spaceIndex = command.IndexOf(' ');
			var baseCommand = command.ToLowerInvariant().Substring(0, spaceIndex > 0 ? spaceIndex : command.Length);
			try {
				switch (baseCommand) {
					case "place":
						PlaceRobot(robot, command);
						break;
					case "move":
						robot.Move();
						break;
					case "left":
						robot.Left();
						break;
					case "right":
						robot.Right();
						break;
					case "report":
						Console.WriteLine(">> " + robot.Report());
						break;
					default:
						Console.WriteLine(">> Please, use only valid commands: {0}", String.Join("; ", _commands));
						break;
				}
			} catch (RobotNotOnTableException) {
				Console.WriteLine(">> '{0}' command ignored. The robot is not on the table.", command);
			} catch (InvalidMovimentException) {
				Console.WriteLine(">> '{0}' command ignored. The robot would fall from the table.", command);
			}
		}
		
		static void PlaceRobot(Robot robot, string command)
		{
			var parameters = command.Substring(command.IndexOf(' ')).Split(',');
			var x = int.Parse(parameters[0]);
			var y = int.Parse(parameters[1]);
			var f = parameters[2].ToUpperInvariant();
			Direction direction = Direction.North;
			
			switch (f) {
				case "N":
					direction = Direction.North;
					break;
				case "E":
					direction = Direction.East;
					break;
				case "S":
					direction = Direction.South;
					break;
				case "W":
					direction = Direction.West;
					break;
			}
			
			robot.Place(x, y, direction);
		}
		
		static void ReadCommandsFile()
		{
			Console.WriteLine("Type the path of the file with the commands:");
			var path = Console.ReadLine();
			var commands = new string[1];
			
			try {
				commands = System.IO.File.ReadAllLines(path);
			} catch {
				Console.WriteLine("Please inform a valid file path.\r\n");
				ReadCommandsFile();
			}
			
			var robot = new Robot();
			foreach (var command in commands) {
				ExecuteCommand(robot, command);
			}
			
			Console.WriteLine("\r\nType anything to quit.");
			Console.ReadKey();
		}
	}
}
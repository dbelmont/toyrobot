﻿using System;
using NUnit.Framework;
using ToyRobot.Exceptions;

namespace ToyRobot.Test
{
	[TestFixture]
	public class ToyRobotTest
	{
		[Test]
		public void RobotConstructorTest_Success()
		{
			var robot = new Robot();
			Assert.That(robot.TableXDimension, Is.EqualTo(5));
			Assert.That(robot.TableYDimension, Is.EqualTo(5));
		}
		
		[Test]
		public void RobotConstructorTest_Failure_InvalidTableXDimension()
		{
			Assert.Throws<InvalidTableDimensionException>(() => new Robot(-1, 5));
		}
		
		[Test]
		public void RobotConstructorTest_Failure_InvalidTableYDimension()
		{
			Assert.Throws<InvalidTableDimensionException>(() => new Robot(2, 0));
		}
		
		[Test]
		public void PlaceRobotTest_Success()
		{
			var robot = new Robot();
			robot.Place(1, 2, Direction.East);
			
			Assert.That(robot.X, Is.EqualTo(1));
			Assert.That(robot.Y, Is.EqualTo(2));
			Assert.That(robot.FaceDirection, Is.EqualTo(Direction.East));
		}
		
		[Test]
		public void PlaceRobot_Failure_OutOfTheTable_NegativeValue()
		{
			var robot = new Robot(6, 6);
			Assert.Throws<InvalidMovimentException>(() => robot.Place(-1, 6, Direction.South));
		}
		
		
		[Test]
		public void PlaceRobot_Failure_OutOfTheTable_OutsideTheTable()
		{
			var robot = new Robot(6, 6);
			Assert.Throws<InvalidMovimentException>(() => robot.Place(0, 7, Direction.South));
		}
		
		[Test]
		[TestCase(Direction.North, new [] {2, 3}, TestName="MoveRobotToEast_Success")]
		[TestCase(Direction.East, new [] {3, 2}, TestName="MoveRobotToEast_Success")]
		[TestCase(Direction.South, new [] {2, 1}, TestName="MoveRobotToEast_Success")]
		[TestCase(Direction.West, new [] {1, 2}, TestName="MoveRobotToEast_Success")]
		public void MoveRobot_Success(Direction faceDirection, int[] expectedResult)
		{
			var robot = new Robot();
			robot.Place(2, 2, faceDirection);
			robot.Move();
			Assert.That(robot.X, Is.EqualTo(expectedResult[0]));
			Assert.That(robot.Y, Is.EqualTo(expectedResult[1]));
		}
		
		[Test]
		public void MoveRobot_Failure_NegativeValue()
		{
			var robot = new Robot();
			robot.Place(0, 0, Direction.South);
			Assert.Throws<InvalidMovimentException>(robot.Move);
		}
		
		[Test]
		public void MoveRobot_Failure_OutsideTheTable()
		{
			var robot = new Robot();
			robot.Place(5, 5, Direction.East);
			Assert.Throws<InvalidMovimentException>(robot.Move);
		}
		
		[Test]
		public void MoveRobot_Failure_RobotNotOnTable()
		{
			var robot = new Robot();
			Assert.Throws<RobotNotOnTableException>(robot.Move);
		}
		
		[Test]
		public void TurnRobotLeft_Success()
		{
			var robot = new Robot();
			robot.Place(2, 2, Direction.East);
			robot.Left();
			Assert.That(robot.FaceDirection, Is.EqualTo(Direction.North));
		}
		
		[Test]
		public void TurnRobotLeftFromNorth_Success()
		{
			var robot = new Robot();
			robot.Place(2, 2, Direction.North);
			robot.Left();
			Assert.That(robot.FaceDirection, Is.EqualTo(Direction.West));
		}
		
		[Test]
		public void TurnRobotLeft_Failure_RobotNotOnTable()
		{
			var robot = new Robot();
			Assert.Throws<RobotNotOnTableException>(robot.Left);
		}
		
		
		[Test]
		public void TurnRobotRight_Success()
		{
			var robot = new Robot();
			robot.Place(2, 2, Direction.East);
			robot.Right();
			Assert.That(robot.FaceDirection, Is.EqualTo(Direction.South));
		}
		
		[Test]
		public void TurnRobotRightFromWest_Success()
		{
			var robot = new Robot();
			robot.Place(2, 2, Direction.West);
			robot.Right();
			Assert.That(robot.FaceDirection, Is.EqualTo(Direction.North));
		}
		
		[Test]
		public void TurnRobotRight_Failure_RobotNotOnTable()
		{
			var robot = new Robot();
			Assert.Throws<RobotNotOnTableException>(robot.Right);
		}
		
		[Test]
		public void Report_Success()
		{
			var robot = new Robot();
			robot.Place(1, 2, Direction.East);
			Assert.That(robot.Report(), Is.EqualTo("1,2,EAST"));
		}
		
		[Test]
		public void Report_Failure_RobotNotOnTable()
		{
			var robot = new Robot();
			Assert.Throws<RobotNotOnTableException>(() => robot.Report());
		}
	}
}
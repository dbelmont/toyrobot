﻿using System;

namespace ToyRobot.Exceptions
{
	public class InvalidMovimentException : Exception
	{
		public InvalidMovimentException() : base("Invalid moviment. This will make the robot fall from the table.") {}
	}
}

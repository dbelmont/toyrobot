﻿using System;

namespace ToyRobot.Exceptions
{
	public class RobotNotOnTableException : Exception
	{
		public RobotNotOnTableException() : base("The robot is not on the table. Use the 'Place' command before any other command.") { }
	}
}
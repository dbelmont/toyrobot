﻿using System;

namespace ToyRobot.Exceptions
{
	public class InvalidTableDimensionException : Exception
	{
		public InvalidTableDimensionException() : base("Invalid table dimensions. Please, use positive integer values to define the table dimensions.") { }
	}
}
﻿using System;

namespace ToyRobot
{
	public enum Direction
	{
		North,
		East,
		South,
		West
	}
	
	public interface IToyRobot
	{
		int TableXDimension { get; }
		int TableYDimension { get; }
		
		int X { get; }
		int Y { get; }
		Direction FaceDirection { get; }
		
		void Place(int x, int y, Direction faceDirection);
		void Move();
		void Left();
		void Right();
		string Report();
	}
}
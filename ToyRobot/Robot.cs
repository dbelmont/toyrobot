﻿using System;
using ToyRobot.Exceptions;

namespace ToyRobot
{
	public class Robot : IToyRobot
	{
		bool _robotPlaced;
		
		public int TableXDimension { get; private set; }
		public int TableYDimension { get; private set; }
		
		public int X { get; private set; }
		public int Y { get; private set; }
		public Direction FaceDirection { get; private set; }
		
		public Robot() : this(5, 5) { }
		public Robot(int tableXDimension, int tableYDimension)
		{
			if (tableXDimension <= 0 || tableYDimension <= 0) throw new InvalidTableDimensionException();
			
			TableXDimension = tableXDimension;
			TableYDimension = tableYDimension;
		}
		
		public void Place(int x, int y, Direction faceDirection)
		{
			if (x < 0 || y < 0 || x > TableXDimension || y > TableYDimension) throw new InvalidMovimentException();
			
			X = x;
			Y = y;
			FaceDirection = faceDirection;
			_robotPlaced = true;
		}
		
		public void Move()
		{
			VerifyRobotIsOnTable();
			
			if ((X == 0 && FaceDirection == Direction.West)
			    || (Y == 0 && FaceDirection == Direction.South)
			    || (X == TableXDimension && FaceDirection == Direction.East)
			    || (Y == TableYDimension && FaceDirection == Direction.North))
				throw new InvalidMovimentException();
			
			//These nested 'ifs' were left like this because, in this specific case, I believe it helps with the readability of the code.
			if (FaceDirection == Direction.North) Y++;
			else if (FaceDirection == Direction.East) X++;
			else if (FaceDirection == Direction.South) Y--;
			else if (FaceDirection == Direction.West) X--;
		}
		
		public void Left()
		{
			VerifyRobotIsOnTable();
			
			if (FaceDirection == Direction.North) FaceDirection = Direction.West;
			else FaceDirection--;
		}
		
		public void Right()
		{
			VerifyRobotIsOnTable();
			
			if (FaceDirection == Direction.West) FaceDirection = Direction.North;
			else FaceDirection++;
		}
		
		private void VerifyRobotIsOnTable()
		{
			if (!_robotPlaced) throw new RobotNotOnTableException();
		}
		
		public string Report()
		{
			VerifyRobotIsOnTable();
			return String.Format("{0},{1},{2}", X, Y, FaceDirection.ToString().ToUpper());
		}
	}
}